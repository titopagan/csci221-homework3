#include <iostream>
#include <unistd.h>
using namespace std;
void fillSpot(bool** grid, int row, int col);
void emptySpot(bool** grid, int row, int col);
bool getCell(bool** grid, int row, int col);
bool** makeGrid();
bool** generation();
bool checkAround(bool** grid, int row, int col);
bool** lifeCycle(bool **grid);
int row_length = 30;
int column_length = 30;

void display(bool **grid)
{
	for(int i = 0; i < row_length; i++)
	{
		for(int j = 0; j < column_length; j++)
		{
			if(getCell(grid, i, j))
			{
				if(j == column_length - 1)
				{
					cout << "|x|" << endl;
				}
				else
				{
					cout << "|x";
				}
			}
			else
			{
				if(j == column_length - 1)
				{
					cout << "| |" << endl;
				}
				else
				{
					cout << "| ";
				}
			}
		}
	}
}

void fillSpot(bool** grid, int row, int col)
{
	if(((row) < 0 || (row) > row_length - 1) || ((col) < 0 || (col) > column_length - 1))
	{
		return;
	}
	*(*(grid + row) + col) = true;
}

void emptySpot(bool** grid, int row, int col)
{
	if(((row) < 0 || (row) > row_length - 1) || ((col) < 0 || (col) >  column_length - 1))
	{
		return;
	}
	*(*(grid + row) + col) = false;
}

bool getCell(bool** grid, int row, int col)
{
	if(((row) < 0 || (row) > row_length - 1) || ((col) < 0 || (col) > column_length - 1))
	{
		return false;
	}
	return *(*(grid + row) + col);
}

bool** makeGrid()
{
	bool **grid = new bool*[row_length];
	for(int row = 0; row < row_length; row++)
	{
		*(grid + row) = new bool[column_length];
		
		for(int col = 0; col < column_length; col++)
		{
			*(*(grid + row) + col) = false;
		}
	}
	return grid;
}

bool** generation()
{
	bool **grid = makeGrid();
		
	fillSpot(grid, 1, 2);
	fillSpot(grid, 1, 4);
	fillSpot(grid, 2, 3);
	fillSpot(grid, 2, 4);
	fillSpot(grid, 3, 3);
	
	fillSpot(grid, 2, 9);
	fillSpot(grid, 2, 10);
	fillSpot(grid, 3, 9);
	fillSpot(grid, 4, 12);
	fillSpot(grid, 5, 11);
	fillSpot(grid, 5, 12);
	
	fillSpot(grid, 2, 14);
	fillSpot(grid, 2, 15);
	fillSpot(grid, 2, 16);
	fillSpot(grid, 1, 15);
	fillSpot(grid, 1, 16);
	fillSpot(grid, 1, 17);
	
	fillSpot(grid, 1, 20);
	fillSpot(grid, 2, 20);
	fillSpot(grid, 3, 20);
	
	fillSpot(grid, 1, 25);
	fillSpot(grid, 1, 27);
	fillSpot(grid, 2, 25);
	fillSpot(grid, 2, 26);
	fillSpot(grid, 3, 26);
	
	fillSpot(grid, 8, 2);
	fillSpot(grid, 9, 2);
	fillSpot(grid, 10, 2);
	
	fillSpot(grid, 6, 23);
	fillSpot(grid, 6, 24);
	fillSpot(grid, 7, 23);
	fillSpot(grid, 7, 22);
	fillSpot(grid, 8, 23);
	
	fillSpot(grid, 8, 27);
	fillSpot(grid, 9, 27);
	fillSpot(grid, 10, 27);
	
	fillSpot(grid, 10, 13);
	fillSpot(grid, 11, 7);
	fillSpot(grid, 11, 8);
	fillSpot(grid, 12, 8);
	fillSpot(grid, 12, 12);
	fillSpot(grid, 12, 13);
	fillSpot(grid, 12, 14);
	
	fillSpot(grid, 12, 19);
	fillSpot(grid, 13, 21);
	fillSpot(grid, 14, 18);
	fillSpot(grid, 14, 19);
	fillSpot(grid, 14, 22);
	fillSpot(grid, 14, 23);
	fillSpot(grid, 14, 24);
	
	fillSpot(grid, 15, 2);
	fillSpot(grid, 15, 3);
	fillSpot(grid, 16, 2);
	fillSpot(grid, 17, 5);
	fillSpot(grid, 18, 4);
	fillSpot(grid, 18, 5);
	
	fillSpot(grid, 26, 2);
	fillSpot(grid, 26, 4);
	fillSpot(grid, 27, 2);
	fillSpot(grid, 27, 3);
	fillSpot(grid, 28, 3);
	
	fillSpot(grid, 17, 27);
	fillSpot(grid, 17, 28);
	fillSpot(grid, 18, 28);
	fillSpot(grid, 19, 25);
	fillSpot(grid, 20, 25);
	fillSpot(grid, 20, 26);
	
	fillSpot(grid, 16, 10);
	fillSpot(grid, 16, 11);
	fillSpot(grid, 16, 12);
	fillSpot(grid, 16, 16);
	fillSpot(grid, 16, 17);
	fillSpot(grid, 16, 18);
	fillSpot(grid, 18, 8);
	fillSpot(grid, 18, 13);
	fillSpot(grid, 18, 15);
	fillSpot(grid, 18, 20);
	fillSpot(grid, 19, 8);
	fillSpot(grid, 19, 13);
	fillSpot(grid, 19, 15);
	fillSpot(grid, 19, 20);
	fillSpot(grid, 20, 8);
	fillSpot(grid, 20, 13);
	fillSpot(grid, 20, 15);
	fillSpot(grid, 20, 20);
	fillSpot(grid, 21, 10);
	fillSpot(grid, 21, 11);
	fillSpot(grid, 21, 12);
	fillSpot(grid, 21, 16);
	fillSpot(grid, 21, 17);
	fillSpot(grid, 21, 18);
	fillSpot(grid, 23, 10);
	fillSpot(grid, 23, 11);
	fillSpot(grid, 23, 12);
	fillSpot(grid, 23, 16);
	fillSpot(grid, 23, 17);
	fillSpot(grid, 23, 18);
	fillSpot(grid, 24, 8);
	fillSpot(grid, 24, 13);
	fillSpot(grid, 24, 15);
	fillSpot(grid, 24, 20);
	fillSpot(grid, 25, 8);
	fillSpot(grid, 25, 13);
	fillSpot(grid, 25, 15);
	fillSpot(grid, 25, 20);
	fillSpot(grid, 26, 8);
	fillSpot(grid, 26, 13);
	fillSpot(grid, 26, 15);
	fillSpot(grid, 26, 20);
	fillSpot(grid, 28, 11);
	fillSpot(grid, 28, 12);
	fillSpot(grid, 28, 13);
	fillSpot(grid, 28, 16);
	fillSpot(grid, 28, 17);
	fillSpot(grid, 28, 18);

	return grid;
}

bool checkAround(bool** grid, int row, int col)
{
	int pop = 0;
	for(int i = -1; i < 2; i++)
	{
		for(int j = -1; j < 2; j++)
		{
			if(((row+i) < 0 || (row+i) > row_length - 1) || ((col+j) < 0 || (col +j) > column_length - 1))
			{
				continue;
			}
			
			if(getCell(grid, (row+i), (col+j)) == true)
			{
				if(i == 0 && j == 0)
				{
					pop--;
				}
				pop++;
			}
		}
	}
	
	if(pop >= 4)
	{
		return false;
	}
	else if(pop <= 1)
	{
		return false;
	}
	else if (pop == 3)
	{
		return true;
	}
	else
	{
		return getCell(grid, row, col);
	}
}

bool** lifeCycle(bool **grid)
{
	bool **derpygrid = makeGrid();
	
	for(int row = 0; row < row_length; row++)
	{
		for(int col = 0; col < column_length; col++)
		{
			if(checkAround(grid, row, col))
			{
				fillSpot(derpygrid, row, col);
			}
			else
			{
				emptySpot(derpygrid, row, col);
			}
		}
	}
	
	for (int i = 0; i < row_length; i++)
	{
		delete[] grid[i];
	}
	
	delete[] grid;
	
	return derpygrid;
}

int main()
{
	bool life = true;
	int lifetime=100;
	bool **life_grid = generation();
	display(life_grid);
	while(life)
	{
		life_grid = lifeCycle(life_grid);
		display(life_grid);
		cout << lifetime << endl;
		cout << endl;
		lifetime--;
		if(lifetime == 0)
		{
			life = false;
		}
		sleep(1);	
	}
	
	for (int i = 0; i < row_length; i++)
	{
		delete[] life_grid[i];
	}
	
	delete life_grid;
}
